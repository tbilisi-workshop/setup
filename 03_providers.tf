
terraform {
  required_version = ">= 0.12"
}

provider "aws" {
  region = "eu-west-2"
  version = "~> 2.50"
}

provider "local" {
  version = "~> 1.4"
}

provider "null" {
  version = "~> 2.1"
}

provider "template" {
  version = "~> 2.1"
}

provider "tls" {
  version = "~> 2.1"
}
