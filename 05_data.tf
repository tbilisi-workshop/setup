
data "aws_ami" "workshop_ubuntu_xenial" {
  most_recent = true
  filter {
    name   = "name"
    values = ["devops-ubuntu-18-04-x64*"]
  }
  owners = ["self"]
}

data "aws_availability_zones" "available" {}
