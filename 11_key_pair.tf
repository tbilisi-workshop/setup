
resource "tls_private_key" "ssh_key" {
  algorithm  = "RSA"
}

resource "aws_key_pair" "workshop_key" {
  key_name   = "workshop_key"
  public_key = tls_private_key.ssh_key.public_key_openssh
}

