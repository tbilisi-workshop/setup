
resource "aws_vpc" "workshop_vpc" {
  cidr_block           = "10.1.0.0/16"
  enable_dns_support   = true
  enable_dns_hostnames = true
  tags = {
    Name = "workshop_vpc"
  }
}

resource "aws_subnet" "public_subnet" {
  count                   = length(data.aws_availability_zones.available.names)
  availability_zone       = data.aws_availability_zones.available.names[count.index]
  cidr_block              = "10.1.${count.index + 100}.0/24"
  map_public_ip_on_launch = true
  vpc_id                  = aws_vpc.workshop_vpc.id
  tags = {
    Name = "subnet_public_${count.index + 1}"
    Access = "public"
  }
}

resource "aws_subnet" "private_subnet" {
  count                   = length(data.aws_availability_zones.available.names)
  availability_zone       = data.aws_availability_zones.available.names[count.index]
  cidr_block              = "10.1.${count.index}.0/24"
  map_public_ip_on_launch = false
  vpc_id                  = aws_vpc.workshop_vpc.id
  tags = {
    Name = "subnet_private_${count.index + 1}"
    Access = "private"
  }
}

resource "aws_default_security_group" "default" {
  vpc_id = aws_vpc.workshop_vpc.id
}

resource "aws_security_group" "workshop_security" {
  name        = "workshop_security"
  description = "Workshop port openings"
  vpc_id      = aws_vpc.workshop_vpc.id

  #
  # common ports
  #

  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  ingress {
    from_port   = 8080
    to_port     = 8080
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  #
  # elasticsearch
  #

  ingress {
    from_port   = 9200
    to_port     = 9200
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  ingress {
    from_port   = 9300
    to_port     = 9300
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  ingress {
    from_port   = 5601
    to_port     = 5601
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  ingress {
    from_port   = 1024
    to_port     = 65535
    protocol    = "tcp"
    cidr_blocks = [aws_vpc.workshop_vpc.cidr_block]
  }

  #
  # outgoing traffic
  #

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "aws_internet_gateway" "workshop_igw" {
  vpc_id = aws_vpc.workshop_vpc.id
}

resource "aws_route_table" "workshop_routing" {
  vpc_id = aws_vpc.workshop_vpc.id
  tags = {
    Name = "workshop_internet"
  }
  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.workshop_igw.id
  }
}

resource "aws_main_route_table_association" "workshop_routing_a" {
  vpc_id         = aws_vpc.workshop_vpc.id
  route_table_id = aws_route_table.workshop_routing.id
}

resource "aws_db_subnet_group" "rds_subnets" {
  name       = "db_subnet_group"
  subnet_ids = aws_subnet.private_subnet.*.id
  tags = {
    Name = "db_subnet_group"
  }
}

resource "aws_eip" "nat_ip" {
  count = length(data.aws_availability_zones.available.names)
  tags = {
    Name = "workshop_nat_ip"
  }
}

resource "aws_nat_gateway" "workshop_nat_gw" {
  count         = length(data.aws_availability_zones.available.names)
  allocation_id = aws_eip.nat_ip[count.index].id
  subnet_id     = aws_subnet.public_subnet[count.index].id
  depends_on    = [ aws_internet_gateway.workshop_igw ]
}

resource "aws_route_table" "private_routing" {
  count  = length(aws_nat_gateway.workshop_nat_gw)
  vpc_id = aws_vpc.workshop_vpc.id
  route {      
    cidr_block = "0.0.0.0/0"
    nat_gateway_id = aws_nat_gateway.workshop_nat_gw[count.index].id
  }
  tags = {
    Name = "workshop_nat"
  }
}

resource "aws_route_table_association" "private_routing" {
  count          = length(aws_route_table.private_routing)
  subnet_id      = aws_subnet.private_subnet[count.index].id
  route_table_id = aws_route_table.private_routing[count.index].id
}
